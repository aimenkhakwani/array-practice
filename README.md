# _Array Practice_

### _Takes user input and pushes it into arrays for practice, August 10, 2016_

#### _**By Aimen Khakwani and Ethan Law**_

## Description

_A site to practice using JavaScript Arrays._

##Setup and Installation

* _Clone_
* _Run in browser_

## Technologies Used

_HTML, CSS, Bootstrap, jQuery, and JavaScript_

### License
Copyright (c) 2016 **_Aimen Khakwani & Ethan Law_**
